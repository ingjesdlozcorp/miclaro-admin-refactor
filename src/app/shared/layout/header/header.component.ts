import { Component, OnInit } from '@angular/core';
import { constants } from 'src/environments/constants';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  imgUrl: string =  `${constants.DeployAssetsPath}/assets/images/pr-flag.png`
  constructor() { }

  ngOnInit(): void {
  }

}
