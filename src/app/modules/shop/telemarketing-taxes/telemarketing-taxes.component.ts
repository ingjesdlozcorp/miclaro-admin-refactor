import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Router} from "@angular/router";
import { ShopService } from 'src/app/core/services/api/shop.service';

declare let alertify: any;

@Component({
  selector: 'app-telemarketing-taxes',
  templateUrl: './telemarketing-taxes.component.html',
  styleUrls: ['./telemarketing-taxes.component.scss']
})
export class TelemarketingTaxesComponent implements OnInit {
  searchForm = new FormGroup({
    ban: new FormControl('', [Validators.required, Validators.maxLength(9), Validators.minLength(9)]),
    subscriber: new FormControl('', [Validators.pattern('/^\d{9}$/')]),
    tax: new FormControl('', [Validators.required])
  });
  subscriberError : boolean = false
  taxError: boolean = false;
  wrongTaxamount: boolean = false;
  title = 'Telemercadeo';

  user = '';
  nameValue = '';
  emailValue = '';
  accounts = '';
  ban = '';
  subscriber = '';
  tax = '';

  constructor(private router: Router, private shopService: ShopService) { }

  ngOnInit(): void {
  }
  
  changeTax() {
    this.wrongTaxamount = false;
    this.taxError = false;
    if (!this.searchForm.value.tax.startsWith('$')) {
      this.searchForm.get('tax').setValue([`$${this.searchForm.value.tax}`]);
    }
    let taxString = this.searchForm.value.tax[0];
    let tax = Number(taxString.substring(1,taxString.length))
    if (  tax < 5 || tax > 500 ) {
      this.wrongTaxamount = true;
    }
    tax.toString() === 'NaN'? this.taxError = true : this.taxError = false;
     if (this.taxError || this.wrongTaxamount) {
      this.searchForm.get('tax').setValue(['']);
     }
  }

  changeSubscriber() {
    this.subscriberError = false;
    let subscriber = this.searchForm.value.subscriber;
    console.log(subscriber)
    if (!this.searchForm.value.subscriber.startsWith('787') && !this.searchForm.value.subscriber.startsWith('989')) {
      this.subscriberError = true;
      this.searchForm.get('subscriber').setValue(['']);
    }
  }

  submitForm(){
    let confirmMessage = 'Esta opción le enviará al cliente un enlace para poder pagar los impuestos generados por compras en Telemarketing a través de la aplicación "Collection Path", ¿está seguro que desea continuar?';
    alertify.confirm(confirmMessage,
      ()=>{
        alertify.success('Procesando...');
        this.generateURL();
      },
      ()=>{
        alertify.error('Cancel');
      });
  }
  
  generateURL() {
    // para el mock: transactionType: INSTALLMENT
    // prod transactionType: 'TELEMARKETING_PAY_TAXES',
    let request = {
      transactionType: 'TELEMARKETING_PAY_TAXES',
      source: 'MI_CL_ADMIN',
      userId: '',
      accountNumber: this.searchForm.value.ban,
      accountType: '',
      accountSubType: '',
      subscriber: this.searchForm.value.subscriber,
      amount: this.searchForm.value.tax[0].substring(1,this.searchForm.value.tax[0].length),
      referenceId: '',
      userNameAudit:JSON.parse(sessionStorage.getItem('user')).userName
    };

    this.shopService.genURLTelemarketing(request).subscribe(
      (response)=> {
        console.log(response);
      },
      error => {
        console.log(error);
      }
    )

  }

   
}


/*

(function() {
    'use strict';

    angular
        .module('app.telemarketing-taxes')
        .controller('TelemarketingTaxesController', TelemarketingTaxesController);

    TelemarketingTaxesController.$inject = ['$rootScope', '$filter', 'dataservice', 'logger',
      '$sce', 'claroServices', '$sessionStorage', '$state', '$timeout', '_',
      'SERVICE_ENTRY', '$modal', '$scope', '$q', 'util', 'ACCOUNTTYPE'];
    function TelemarketingTaxesController($rootScope, $filter, dataservice, logger,
      $sce, claroServices, $sessionStorage, $state, $timeout, _,
      SERVICE_ENTRY, $modal, $scope, $q, util, ACCOUNTTYPE) {

        //Variables
        var vm = this;
        vm.promises = []; //stores all promises
        vm.sendDescription = false;
        vm.title = 'Telemercadeo';
        vm.sortBy = ['-visible'];
        vm.changeStatus = [];
        vm.listAccounts = [];
        vm.searchButtonTitle = 'Buscar';
        vm.submitForm = submitForm;
        vm.formSubmited = false;
        vm.generateURL = generateURL;

        vm.user = '';
        vm.nameValue = '';
        vm.emailValue = '';
        vm.accounts = '';
        vm.ban = '';
        vm.subscriber = '';
        vm.tax = '';
        vm.cleanForm = cleanForm;

        ///////////////////
        activate();
        ///////////////////
        function submitForm(form) {
            vm.formSubmited = true;
              // Validate form
              if (form.$valid && (vm.tax >= 5.0 && vm.tax <= 500.0)) {
                var confirmMessage = 'Esta opción le enviará al cliente un enlace para poder pagar los impuestos generados por compras en Telemarketing a través de la aplicación "Collection Path", ¿está seguro que desea continuar?';
                alertify.set({
                    labels: {
                        ok: 'SI',
                        cancel: 'NO'
                    }
                });
                alertify.confirm(confirmMessage, function(e) {
                    if (e) {
                      generateURL();
                    }
                });
              }
        }

        function activate() {
            $timeout(function () {
                $rootScope.$broadcast('menu-selected-updated', 4);
            });
        }

        function generateURL() {
            
            

            var request = {
                'transactionType': 'TELEMARKETING_PAY_TAXES',
                'source': 'MI_CL_ADMIN',
                'userId': '',
                'accountNumber': vm.ban,
                'accountType': '',
                'accountSubType': '',
                'subscriber': vm.subscriber,
                'amount': vm.tax,
                'referenceId': '',
                'userNameAudit': $sessionStorage.user.userName
            };

            


            return claroServices.reports.genURLTelemarketing(request)
                .then(function(data) {
                  if (!data.hasError) {
                      alertify.set({labels: {ok: 'OK'}});
                      alertify.alert('Mensaje con URL fue enviado.');
                      
                      vm.cleanForm();
                  }else {
                      alertify.set({labels: {ok: 'OK'}});
                      alertify.alert('Error');
                  }
                })
                .catch(function(error) {
                    alertify.set({labels: {ok: 'OK'}});
                    alertify.alert(error);
            });

        }

        function cleanForm(){
          vm.formSubmited = false;
          vm.ban = '';
          vm.subscriber = '';
          vm.tax = '';
        }
    }
})();


*/