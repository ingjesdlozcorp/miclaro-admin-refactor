import { Component, OnInit } from '@angular/core';
import { constants } from 'src/environments/constants';
import {Router} from "@angular/router";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/core/services/api/user.service';
import { LoginResponse } from 'src/app/core/interfaces/models';
declare let alertify: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  imgUrl: string =  `${constants.DeployAssetsPath}/assets/images/`;
  flagUrl: string = this.imgUrl + 'pr-flag.png';
  logoUrl: string = this.imgUrl + 'miclaro-logo.png';
  bannerUrl: string = this.imgUrl + 'log-banner.jpg';
  iconUrl: string = this.imgUrl +'claro-icon.png';
  title : string = 'Inicio';
  subtitle : string = 'Login';
  rememberLogin: any = localStorage.getItem('rememberLogin');
  loginForm = new FormGroup({
    user: new FormControl('', [Validators.required, Validators.minLength(10)]),
    password: new FormControl('', [Validators.required, Validators.email]),
    remember: new FormControl(false)
  });

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit(): void {
    if (sessionStorage.getItem('userName') !== '') {
      if (sessionStorage.getItem('user')) {
        this.router.navigate(['/shop'], { replaceUrl: true });
      }
    }
  }

  activate() {
    setTimeout(() => {
      console.log('estoy activando/desactivando: ', this.loginForm.value.remember);
    if (this.loginForm.value.remember) {
      sessionStorage.setItem('userName', this.loginForm.value.user);
      sessionStorage.setItem('password', this.loginForm.value.password);
      localStorage.setItem('rememberLogin', 'true');
    } else {
      sessionStorage.removeItem('userName');
      sessionStorage.removeItem('password');
      localStorage.removeItem('rememberLogin');
    }
    this.rememberLogin = localStorage.getItem('rememberLogin');
    }, 200)
    
  }

  submitForm() {
    if (this.loginForm.value.user === '' || this.loginForm.value.password === '') {
      alertify.alert('Debe ingresar su usuario y contraseña.');
      return;
    }
    if (this.rememberLogin) {
      sessionStorage.setItem('userName', this.loginForm.value.user);
    } else {
      sessionStorage.removeItem('userName');
    }
    this.userService.login(this.loginForm.value.user, this.loginForm.value.password, 'loginad').subscribe(
      (response) => {
        let userPermissions = [];
        let codLoginExp = '045';
        console.log(response['body']);
        let data : LoginResponse = response['body']
        
        if (!data.hasError) {
          if (data.code === codLoginExp) {
            alertify.alert('Su contraseña ha expirado, por favor comuníquese con su Administrador');
          } else {
            sessionStorage.setItem('user', JSON.stringify(data.object));
            sessionStorage.setItem('wsp', this.loginForm.value.password);

            if (this.rememberLogin) {
              this.loginForm.value.remember = true
            } else {
              this.loginForm.value.remember = false
            }
            let permissions : any [] = data.object.permissions;
            // format user permissions
            permissions.forEach((permission)  => {
              userPermissions.push(permission.id);
            });

            if (data.object.roles[0].roleId === 'MCA-VENTAS') {
              this.router.navigate(['shop'], { replaceUrl: true });
            } else {
              alertify.alert('Usuario Inválido');
            }
          }

        }
        
      },
      error => {
        if (error === 'El usuario esta bloqueado.') {
          alertify.alert('Los usuarios con roles Administrativos o de Servicio al Cliente deben comunicarse con el Departamento de Seguridad para reactivar su cuenta.');
        } else {
          alertify.alert('No se ha podido inciar sesi&oacute;n con el usuario y contrase&ntilde;a indicada');
        }
      }
    )
  }

}
