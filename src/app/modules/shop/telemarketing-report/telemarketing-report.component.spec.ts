import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TelemarketingReportComponent } from './telemarketing-report.component';

describe('TelemarketingReportComponent', () => {
  let component: TelemarketingReportComponent;
  let fixture: ComponentFixture<TelemarketingReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TelemarketingReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TelemarketingReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
