import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TelemarketingTaxesComponent } from './telemarketing-taxes.component';

describe('TelemarketingTaxesComponent', () => {
  let component: TelemarketingTaxesComponent;
  let fixture: ComponentFixture<TelemarketingTaxesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TelemarketingTaxesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TelemarketingTaxesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
