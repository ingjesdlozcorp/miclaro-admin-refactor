import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopComponent } from './shop.component';
import { TelemarketingTaxesComponent } from './telemarketing-taxes/telemarketing-taxes.component';
import { InstallmentTaxesComponent } from './installment-taxes/installment-taxes.component';
import { ShopRoutingModule } from './shop-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { TelemarketingReportComponent } from './telemarketing-report/telemarketing-report.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ShopComponent,
    TelemarketingTaxesComponent,
    InstallmentTaxesComponent,
    TelemarketingReportComponent
  ],
  imports: [
    CommonModule,
    ShopRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class ShopModule { }
