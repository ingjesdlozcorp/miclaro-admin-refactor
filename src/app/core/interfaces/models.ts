export interface LoginResponse {
    response: boolean;
    hasError: boolean;
    desc: string;
    code: string;
    executeDate: string;
    object: ObjectLoginResponse;
}

interface ObjectLoginResponse {
    userName: string;
    alerts: any[];
    roles: Role []
    accounts: any [];
    permissions: Permission[];
    sessionId: string;
}
interface Role {
    roleId: string;
    name: string;
}
interface Permission {
    id: string
    name: string;
    enabled: string;
    module: string;
}
