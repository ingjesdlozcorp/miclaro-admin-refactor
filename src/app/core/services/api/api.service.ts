import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { constants } from '../../../../environments/constants';



@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor( private http: HttpClient) { }

  runQuery( selectedUrl: string, additionalUrlFragment: string, typeHttpRequest: string, data? ) {
    let query: string = this.getUrl(selectedUrl) + additionalUrlFragment;
    return this.selectMethod(query, typeHttpRequest, data);
  }

  selectMethod(query: string, typeHttpRequest: string, data? ) {
    if (typeHttpRequest === 'get') {
      return this.runQueryGet(query);
    } else if(typeHttpRequest === 'patch') {
      return this.runQueryPatch (data, query);
    } else {
      return this.runQueryPost(data, query)
    }
  }

  runQueryGet(query) {
    return this.http.get( query );
  }

  runQueryPatch(data, query)  {
    return this.http.patch(query, data, { observe: 'response' });
  }

  runQueryPost(data, query) {
    return this.http.post(query, data, { observe: 'response' });
  }

  getUrl (selectedUrl) {
    switch (selectedUrl) {
      case 'ENTRY_INSTALLMENT':
        return constants.MOCK_API? constants.MOCK_API_PATH + '/loginad' :constants.SERVICE_ENTRY_INSTALLMENT;
        break
      case 'SERVICE_ENTRY':
        return constants.MOCK_API? constants.MOCK_API_PATH  : constants.SERVICE_ENTRY;
        break
      default:
        // por modificar
        return constants.MOCK_API? constants.MOCK_API_PATH + '/loginad' : constants.API_PATH_TELEMARQUETING;
        break
    }
  }
  

}