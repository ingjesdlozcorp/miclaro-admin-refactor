import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})

/**
 * shop services here
 */
export class ShopService {

  constructor(private apiService: ApiService) { }

  genURLTelemarketing(params) {
   return this.apiService.runQuery('SERVICE_ENTRY', '/url/generate', 'post', params);
  }
}
