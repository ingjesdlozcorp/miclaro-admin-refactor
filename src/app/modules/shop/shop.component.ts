import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { constants } from 'src/environments/constants';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {
  title : string = 'Tienda en Línea';
  subtitle: string = 'Resumen';
  imgUrl: string =  `${constants.DeployAssetsPath}/assets/`;
  icoImg: string = this.imgUrl + 'images/ico_vales-educativos.png'
  menuElements = [
    {
        title: 'Telemercadeo',
        description: 'Opción para cargar el Impuesto generado a través de una venta por Telemercadeo',
        image: this.imgUrl +'images/telemarket-icon.png',
        sref: 'telemarketing-taxes',
    },
    {
        title: 'Reporte de Telemercadeo',
        description: 'Listado de enlaces enviados para Pago de Impuestos por ventas en Telemercadeo.',
        image: this.imgUrl +'images/telemarket-r2.png',
        sref: 'telemarketing-report',
    },
    {
        title: 'Acceleración de Installments',
        description: '',
        image: this.imgUrl +'images/telemarket-icon.png',
        sref: 'installment-taxes',
    }
];
  constructor(private router: Router) { }

  ngOnInit(): void {
    //sessionStorage.getItem()
  }

  goTo(url) {
    url = 'shop/' + url;
    this.router.navigate([url], { replaceUrl: true });
  }

}
