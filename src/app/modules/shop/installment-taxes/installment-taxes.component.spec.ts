import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstallmentTaxesComponent } from './installment-taxes.component';

describe('InstallmentTaxesComponent', () => {
  let component: InstallmentTaxesComponent;
  let fixture: ComponentFixture<InstallmentTaxesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstallmentTaxesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstallmentTaxesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
