import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private apiService: ApiService) { }

  login(userName: string, password: string , method: string) {
    let params = {
      username: userName,
      password: password,
      method: method
    };
   return this.apiService.runQuery('ENTRY_INSTALLMENT', '', 'post', params);
  }
  
}
