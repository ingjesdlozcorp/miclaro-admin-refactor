import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InstallmentTaxesComponent } from './installment-taxes/installment-taxes.component';
import { ShopComponent } from './shop.component';
import { TelemarketingReportComponent } from './telemarketing-report/telemarketing-report.component';
import { TelemarketingTaxesComponent } from './telemarketing-taxes/telemarketing-taxes.component';



const routes: Routes = [
  {
      path: '',
      component: ShopComponent
  },
  {
    path: 'installment-taxes',
    component: InstallmentTaxesComponent
  },
  {
    path: 'telemarketing-taxes',
    component: TelemarketingTaxesComponent
  },
  {
    path: 'telemarketing-report',
    component: TelemarketingReportComponent
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShopRoutingModule { }
